# __DFN.Lab__

Welcome to the Gitlab page dedicated to DFN.lab.

## Description

DFN.Lab is a software suite for Discrete Fracture Network analyses. It includes various modules for generation, analyses, flow, transport and mechanics.
See https://www.fractorylab.com for more details.

## Contents

This repository contains tutorials on how to use DFN.Lab, and some advanced application examples. The master branch contains the tutorials for the latest numbered version. If you got an older version, go to the TAG of the version you're using now or ask for an update.

##### Tutorials
0. Introduction
1. Python and DFN.Lab
2. Domains
3. Wells and tunnels
4. Fractures
5. Input and output DFN files
6. Stochastic DFN generation
7. UFM-DFN generation
8. Hydraulic boundary conditions
9. Hydraulic properties
10. Connected structure identification
11. Meshing
12. Flow (steady)
13. Solving flow with graphs
14. Particle tracking

Please keep updated, we are adding new tutorials on a regular basis, covering other applications like site models, inert transport simulations, heat transfert simulations or mechanical properties of fractures rocks.

## The Team

DFN.lab is developed by the *Fractory*, a joint laboratory between Itasca Consultants, the CNRS and the University of Rennes.

## Where is the code

The code is not freely available, please contact r.legoc@itasca.fr or philippe.davy@univ-rennes1.fr to ask for an access. We provide  free access only to collaborators in academic research.

You will find a this page some tutorials on the use of DFN.lab that you can run once you get the code. You can also submit tickets if you have any question/bugs.
